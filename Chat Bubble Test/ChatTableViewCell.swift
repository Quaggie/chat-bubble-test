//
//  ChatTableViewCell.swift
//  Chat Bubble Test
//
//  Created by Jonathan Bijos on 16/04/18.
//  Copyright © 2018 DevsCarioca. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    static func getHeight(message: String, width: CGFloat) -> CGFloat {
        // height constraints
        let topBottomConstraints: CGFloat = 8
        let bubbleHeightInsets: CGFloat = 34
        // width constraints
        let bubbleInsets: CGFloat = 42
        let bubbleWidthConstraints: CGFloat = 16
        
        let height = topBottomConstraints +
            bubbleHeightInsets +
            message.height(withConstrainedWidth: width - bubbleInsets - bubbleWidthConstraints,
                           font: UIFont.systemFont(ofSize: 15))
        
        return height
    }
    
    var message: String? {
        didSet {
            if let message = message {
                messageLabel.text = message
                setupBubble(wasSent: false)
            }
        }
    }
    
    let bubbleImgView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "Bubble"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        return iv
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    var bubbleLeftFixedConstraint: NSLayoutConstraint!
    var bubbleRightFixedConstraint: NSLayoutConstraint!
    
    var bubbleLeftFlexibleConstraint: NSLayoutConstraint!
    var bubbleRightFlexibleConstraint: NSLayoutConstraint!
    
    func setupViews() {
//        backgroundColor = UIColor.lightText
//        backgroundView?.backgroundColor = UIColor.lightText
//        contentView.backgroundColor = UIColor.lightText
        contentView.addSubview(bubbleImgView)
        bubbleImgView.addSubview(messageLabel)
        
        bubbleImgView.anchor(topAnchor, bottom: bottomAnchor, topConstant: 4, bottomConstant: 4)
        
        bubbleLeftFixedConstraint = bubbleImgView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16)
        bubbleRightFixedConstraint = bubbleImgView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16)
        
        bubbleLeftFlexibleConstraint = bubbleImgView.leftAnchor.constraint(greaterThanOrEqualTo: leftAnchor, constant: 8)
        bubbleRightFlexibleConstraint = bubbleImgView.rightAnchor.constraint(lessThanOrEqualTo: rightAnchor, constant: -8)
        
        messageLabel.anchor(bubbleImgView.topAnchor, left: bubbleImgView.leftAnchor,
                            bottom: bubbleImgView.bottomAnchor, right: bubbleImgView.rightAnchor,
                            topConstant: 17, leftConstant: 21, bottomConstant: 17, rightConstant: 21)
    }
    
    func setupBubble(wasSent: Bool) {
        let image: UIImage
        if wasSent {
            image = #imageLiteral(resourceName: "Bubble")
            messageLabel.textColor = .white
            bubbleImgView.tintColor = .black

            bubbleLeftFixedConstraint.isActive = false
            bubbleRightFixedConstraint.isActive = true
            
            bubbleLeftFlexibleConstraint.isActive = true
            bubbleRightFlexibleConstraint.isActive = false
        } else {
            image = #imageLiteral(resourceName: "Bubble")
            messageLabel.textColor = .white
            bubbleImgView.tintColor = UIColor.red
            
            bubbleRightFixedConstraint.isActive = false
            bubbleLeftFixedConstraint.isActive = true
            
            bubbleLeftFlexibleConstraint.isActive = false
            bubbleRightFlexibleConstraint.isActive = true
        }
        
        bubbleImgView.image = image
            .resizableImage(withCapInsets: UIEdgeInsetsMake(17, 21, 17, 21), resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
        layoutIfNeeded()
    }
}
