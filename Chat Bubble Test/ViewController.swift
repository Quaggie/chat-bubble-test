//
//  ViewController.swift
//  Chat Bubble Test
//
//  Created by Jonathan Bijos on 16/04/18.
//  Copyright © 2018 DevsCarioca. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var datasource: [String] = []
    
    let tableView = UITableView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "ChatTableViewCell")
        
        datasource = [
            "Testando chat",
            "Alou alou 123123123 12312312 3123 123 123 123123123123 123123 123123213 123123 123 123 123 12 31 23 123 12 31 23 123 213 123123 123 123 123 12 31 23 123 12 31 23 123213 123123 123 123 123 12 31 23 123 12 31 23 123213 123123 123 123 123 12 31 23 123 12 31 23 123",
            "Mr MrrR Lorem Lorem Lorem Lorem Lorem Lorem"]
        tableView.reloadData()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(removeChatBubbles))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addChatBubble))
    }
    
    @objc func addChatBubble() {
        datasource.append("Mr MrrR Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem")
        let lastPath = IndexPath(row: datasource.count - 1, section: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [lastPath], with: .automatic)
        tableView.endUpdates()
        tableView.scrollToRow(at: lastPath, at: .bottom, animated: true)
    }
    
    @objc func removeChatBubbles() {
        datasource.removeAll()
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        debugPrint(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = datasource[indexPath.row]
        return ChatTableViewCell.getHeight(message: message, width: tableView.frame.width)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        cell.backgroundColor = UIColor.lightText
        cell.message = datasource[indexPath.row]
        return cell
    }
}
